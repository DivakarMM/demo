<!--start page title -->
<div class="row align-items-center">
    <div class="col-sm-6">
        <div class="page-title-box">
            <h4 class="font-size-18">Dashboard</h4>
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item active">Welcome to <?php echo config_item('project_name'); ?> Dashboard</li>
            </ol>
        </div>
    </div>
    <div class="col-md-12">
        <div id="divCalendar"></div>
    </div>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/1.6.4/fullcalendar.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/1.6.4/fullcalendar.css">
    <script>
        $(document).ready(function() {

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            $.ajax({
                url: "<?php echo base_url(); ?>dashboard/get_events/",
                success: function(result) {
                    var e = JSON.parse(result);
                    if (result) {

                        var calendar = $('#divCalendar').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek ,basicDay'
                            },
                            buttonText: {
                                prev: 'prev',
                                next: 'next'
                            },
                            timeFormat: 'h(:mm)t',
                            selectable: true,
                            dayClick: function(date, allDay, jsEvent, view) {
                                alert(date);
                            },
                            eventClick: function(calEvent, jsEvent, view) {
                                alert('Event: ' + calEvent.title);
                                // change the border color just for fun
                                $(this).css('border-color', 'red');

                            },
                            select: function(start, end, allDay) {
                                var title = prompt('Event Title:');
                                if (title) {
                                    calendar.fullCalendar('renderEvent', {
                                            title: title,
                                            start: start,
                                            end: end,
                                            allDay: allDay
                                        },
                                        true // make the event "stick"
                                    );
                                }
                                calendar.fullCalendar('unselect');
                            },

                            events: e
                        });
                    }
                }
            });


            $('.fc-button-prev').click(function() {
                alert(calendar.fullCalendar('removeEvents'));
                calendar.fullCalendar('renderEvent', {
                    title: 'New',
                    start: "2013-09-09 10:30"
                });

            });

        });
    </script>