    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Events</h4>
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Events</a></li>
                    <li class="breadcrumb-item active">Manage Events</li>
                </ol>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="float-right d-none d-md-block">

                <a class="btn btn-primary waves-effect waves-light" href="<?php echo base_url(); ?>users/events/">
                    View Events
                </a>
            </div>
        </div>
    </div>
    <?php
    if ($this->session->flashdata('alert_success')) {
    ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('alert_success'); ?>
        </div>
    <?php
    }

    if ($this->session->flashdata('alert_danger')) {
    ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('alert_danger'); ?>
        </div>
    <?php
    }

    if ($this->session->flashdata('alert_warning')) {
    ?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('alert_warning'); ?>
        </div>
    <?php
    }
    if (validation_errors()) {
    ?>

    <?php
    }
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Create New Events</h4>
                    <form action="<?php echo base_url(); ?>users/events/add" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>From Date</label><span class="mandatory">*</span>
                            <input name="from_date" id="from_date" type="date" class="form-control" placeholder="Enter From Date">
                            <?php if (form_error('first_name')) { ?>
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <?php echo form_error('first_name'); ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            <label>To Date</label><span class="mandatory">*</span>
                            <input name="to_date" id="to_date" type="date" class="form-control" placeholder="Enter To Date">
                            <?php if (form_error('to_date')) { ?>
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <?php echo form_error('to_date'); ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            <label>Event Title</label><span class="mandatory">*</span>
                            <input name="event_title" id="event_title" type="text" class="form-control" placeholder="Event Title">
                            <?php if (form_error('event_title')) { ?>
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <?php echo form_error('event_title'); ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="form-group mb-0">
                            <div>
                                <button name="submit" type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                    Submit
                                </button>
                                <button type="reset" class="btn btn-secondary waves-effect" onclick="window.history.back()">
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>