<div class="row align-items-center">
    <div class="col-sm-6">
        <div class="page-title-box">
            <h4 class="font-size-18">User</h4>
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="<?php echo site_url('users/manage_users'); ?>">User</a></li>
                <li class="breadcrumb-item active">User Management</li>
            </ol>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="float-right d-none d-md-block">
            <input type="button" id="btnExporttoExcel" class="btn btn-primary waves-effect waves-light" value="Export To Excel">

            <a class="btn btn-primary waves-effect waves-light" href="<?php echo base_url(); ?>users/manage_users/add/">
                <i class="mdi mdi-plus mr-2"></i> Add New
            </a>
        </div>
    </div>
</div>
<?php
if ($this->session->flashdata('alert_success')) {
    ?>
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <strong>Success!</strong> <?php echo $this->session->flashdata('alert_success'); ?>
</div>
<?php
}

if ($this->session->flashdata('alert_danger')) {
    ?>
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <strong>Success!</strong> <?php echo $this->session->flashdata('alert_danger'); ?>
</div>
<?php
}

if ($this->session->flashdata('alert_warning')) {
    ?>
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <strong>Success!</strong> <?php echo $this->session->flashdata('alert_warning'); ?>
</div>
<?php
}
?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table id="dmstable" class="table table-striped table-bordered dt-responsive nowrap"
                    style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>USERNAME</th>
                            <th>EMAIL</th>
                            <th>MOBILE</th>
                            <th>CREATED</th>
                            <th>ACTION</th>
                            <th>DELETE</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
datatable_init('<?php echo base_url(); ?>', '<?php echo $datatable; ?>', [
    {
        "data": "username",
    },
    {
        "data": "email",
    },
    {
        "data": "mobile",
    },
    {
        "data": "created_at",
        "render": function(data) {
            if (data) {
                var date = new Date(data),
                    yr = date.getFullYear(),
                    month = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
                    day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),
                    newDate = day + '-' + month + '-' + yr;
                return newDate;
            }
        }
    },
    {
        "data": "action"
    },
    {
        "data": "delete"
    }
]);

function delete_item(id) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        $.ajax({
            url: "<?php echo base_url(); ?>users/manage_users/delete/" + id + "/",
            success: function(result) {
                if (result) {
                    window.location('settings/basic/free_delivery');
                }
            }
        });
        if (result.value) {
            Swal.fire(
                'Deleted!',
                'User has been deleted.',
                'success'
            );
            $('#dmstable').DataTable().ajax.reload();
        }
    });
}
</script>
<script src="<?php echo base_url(); ?>assets/js/table2excel.js" type="text/javascript"></script>
<script type="text/javascript">
$(function() {
    $("#btnExporttoExcel").click(function() {
        $("#dmstable").table2excel({
            filename: "Customer.xls"
        });
    });
});
</script>