<div class="row align-items-center">
    <div class="col-sm-6">
        <div class="page-title-box">
            <h4 class="font-size-18">Customer</h4>
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Customer</a></li>
                <li class="breadcrumb-item active">Customer Management</li>
            </ol>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="float-right d-none d-md-block">

            <a class="btn btn-primary waves-effect waves-light" href="<?php echo base_url(); ?>users/manage_users/">
                View Customer
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">View Customer</h4>
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th>Customer Name</th>
                            <td><?php echo $default['first_name']; ?> <?php echo $default['last_name']; ?></td>
                        </tr>

                        <tr>
                            <th>Phone</th>
                            <td><?php echo $default['mobile']; ?></td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td><?php echo $default['email']; ?></td>
                        </tr>
                        <tr>
                            <th>Home Address</th>
                            <td><?php echo $home['door_no'] . ' ,' . $home['street_name'] . ' ,' . $home['city'] . ' ,' . $home['state'] . ' ,' . $home['pincode']; ?>
                            </td>
                        </tr>

                        <tr>
                            <th>Work Address</th>
                            <td><?php echo $work['door_no'] . ' ,' . $work['street_name'] . ' ,' . $work['city'] . ' ,' . $work['state'] . ' ,' . $work['pincode']; ?>
                            </td>
                        </tr>

                        <tr>
                            <th>Address</th>
                            <td><?php echo $address['door_no'] . ' ,' . $address['street_name'] . ' ,' . $address['city'] . ' ,' . $address['state'] . ' ,' . $address['pincode']; ?>
                            </td>
                        </tr>



                        <tr>
                            <td colspan="2" class="text-center">
                                <!--
                                <a href="<?php echo base_url(); ?>seller/branchmanager/branch_manager/edit/<?php echo $default['user_id']; ?>"
                                    class="btn btn-primary">
                                    Edit Branch
                                </a> -->

                                &nbsp;<a href="<?php echo base_url(); ?>users/manage_users/"
                                    class="btn btn-secondary">Back</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>