<div class="row align-items-center">
    <div class="col-sm-6">
        <div class="page-title-box">
            <h4 class="font-size-18">User Management</h4>
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Users</a></li>
                <li class="breadcrumb-item active">Manage Users</li>
            </ol>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="float-right d-none d-md-block">

            <a class="btn btn-primary waves-effect waves-light" href="<?php echo base_url(); ?>users/manage_users/">
                View Users
            </a>
        </div>
    </div>
</div>

<?php
if ($this->session->flashdata('alert_success')) {
    ?>
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <strong>Success!</strong> <?php echo $this->session->flashdata('alert_success'); ?>
</div>
<?php
}

if ($this->session->flashdata('alert_danger')) {
    ?>
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <strong>Success!</strong> <?php echo $this->session->flashdata('alert_danger'); ?>
</div>
<?php
}

if ($this->session->flashdata('alert_warning')) {
    ?>
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <strong>Success!</strong> <?php echo $this->session->flashdata('alert_warning'); ?>
</div>
<?php
}
if (validation_errors()) {
    ?>
<!-- <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <?php echo validation_errors(); ?>
    </div> -->
<?php
}
?>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Create New User</h4>

                <form action="<?php echo base_url(); ?>users/manage_users/add" method="post"
                    enctype="multipart/form-data">
                    <div class="form-group">
                        <label>First Name</label><span class="mandatory">*</span>
                        <input name="first_name" id="first_name" type="text" class="form-control"
                            placeholder="Enter First Name">
                        <?php if (form_error('first_name')) {?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <?php echo form_error('first_name'); ?>
                        </div>
                        <?php }?>
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input name="last_name" id="last_name" type="text" class="form-control"
                            placeholder="Enter Last Name">

                    </div>
                    <div class="form-group">
                        <label>Email Address</label><span class="mandatory">*</span>
                        <input name="email" id="email" type="email" class="form-control" placeholder="Enter Email">
                        <?php if (form_error('email')) {?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <?php echo form_error('email'); ?>
                        </div>
                        <?php }?>
                    </div>
                    <div class="form-group">
                        <label>Mobile Number</label><span class="mandatory">*</span>
                        <input name="mobile_number" id="mobile_number" type="number" class="form-control"
                            placeholder="Enter Mobile Number" onkeyup="check();">
                        <span id="message"></span>
                        <?php if (form_error('mobile_number')) {?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <?php echo form_error('mobile_number'); ?>
                        </div>
                        <?php }?>
                    </div>
                
                    <div class="form-group">
                        <label>Username</label><span class="mandatory">*</span>
                        <input name="username" id="username" type="text" class="form-control"
                            placeholder="Enter Username">
                        <?php if (form_error('username')) {?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <?php echo form_error('username'); ?>
                        </div>
                        <?php }?>
                    </div>
                    <div class="form-group">
                        <label>Password</label><span class="mandatory">*</span>
                        <input name="password" id="password" type="password" class="form-control"
                            placeholder="Enter Password">
                        <?php if (form_error('password')) {?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <?php echo form_error('password'); ?>
                        </div>
                        <?php }?>
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label><span class="mandatory">*</span>
                        <input name="confirm_password" id="confirm_password" type="password" class="form-control"
                            placeholder="Confirm Password">

                        <?php if (form_error('confirm_password')) {?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <?php echo form_error('confirm_password'); ?>
                        </div>
                        <?php }?>
                    </div>


                    <div class="form-group mb-0">
                        <div>
                            <button name="submit" type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                Submit
                            </button>
                            <button type="reset" class="btn btn-secondary waves-effect" onclick="window.history.back()">
                                Cancel
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<script>
function check() {

    var mobile = document.getElementById('mobile_number');


    var message = document.getElementById('message');

    var goodColor = "#0C6";
    var badColor = "#FF9B37";

    if (mobile.value.length != 10) {
        mobile.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "required 10 digits, match requested format!"
    } else {
        mobile.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = ""
    }
}
</script>