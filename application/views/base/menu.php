    <?php
    $u1 = $this->uri->segment(1);
    $u2 = $this->uri->segment(2);
    ?>
    <div class="vertical-menu">
        <div data-simplebar class="h-100">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <?php if ($this->auth_level != 9) { ?>
                    <ul class="metismenu list-unstyled" id="side-menu">
                        <li class="menu-title">Menu</li>
                            <li>
                                <a href="<?php echo base_url(); ?>" class="waves-effect">
                                    <i class="ti-home"></i>
                                    <span>Dashboard</span>
                                </a>
                            </li>
                        <li>
                            <a href="javascript: void(0);" class="has-arrow waves-effect">
                                <i class="ti-user"></i>
                                <span>Event</span>
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li <?php echo ($u2 == 'events') ? 'class="mm-active"' : ''; ?>>
                                    <a href="<?php echo base_url(); ?>users/events" <?php echo ($u2 == 'events') ? 'class="active"' : ''; ?>>Manage Events</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo base_url('logout'); ?>" class="waves-effect">
                                <i class="ti-na"></i>
                                <span>logout</span>
                            </a>
                        </li>
                    </ul>

                <?php } else { ?>
                    <ul class="metismenu list-unstyled" id="side-menu">
                        <li>
                            <a href="<?php echo base_url(); ?>" class="waves-effect">
                                <i class="ti-home"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript: void(0);" class="has-arrow waves-effect">
                                <i class="ti-user"></i>
                                <span>Users</span>
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li <?php echo ($u2 == 'users') ? 'class="mm-active"' : ''; ?>>
                                    <a href="<?php echo base_url(); ?>users/manage_users" <?php echo ($u2 == 'manage_users') ? 'class="active"' : ''; ?>>Manage Users</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="javascript: void(0);" class="has-arrow waves-effect">
                                <i class="ti-user"></i>
                                <span>Event</span>
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li <?php echo ($u2 == 'events') ? 'class="mm-active"' : ''; ?>>
                                    <a href="<?php echo base_url(); ?>users/events" <?php echo ($u2 == 'events') ? 'class="active"' : ''; ?>>Manage Events</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo base_url('logout'); ?>" class="waves-effect">
                                <i class="ti-na"></i>
                                <span>logout</span>
                            </a>
                        </li>
                    </ul>
                <?php } ?>

            </div>
            <!-- Sidebar -->
        </div>
    </div>