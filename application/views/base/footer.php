<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                &copy; <script>document.write(new Date().getFullYear())</script> <?php echo config_item('project_name'); ?><span class="d-none d-sm-inline-block"> - Crafted with <i class="mdi mdi-heart text-danger"></i> by Divakar.</span>
            </div>
        </div>
    </div>
</footer>