<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->lang->load("app", "english");
    }

    /*
    Divakar AUTH
    */
    public function login()
    {
        if ($this->session->userdata('logged_in')) {
            redirect('dashboard');
        }

        //date_default_timezone_set($this->dbvars->timezone);
        // Method should not be directly accessible
        if ($this->uri->uri_string() == 'auth/login') {
            show_404();
        }

        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
            $this->require_min_level(1);
        }

        $this->setup_login_form();
        $view_data = '';
        $data = array(
            'title' => $this->lang->line('login_page_title'),
            'content' => $this->load->view('auth/login_form', $view_data, true),
        );
        $this->load->view('base/login_template', $data);
    }

    /**
     * Log out
     */
    public function logout()
    {

        $this->session->unset_userdata('logged_in');
        $this->authentication->logout();

        // Set redirect protocol
        $redirect_protocol = USE_SSL ? 'https' : null;

        redirect(site_url(LOGIN_PAGE . '?logout=1', $redirect_protocol));
    }

    // --------------------------------------------------------------

}
