<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->is_logged_in();
        $this->load->model('Events_model', "em");
    }

    public function index()
    {
        if ($this->verify_min_level(1)) {
            $view_data="";
            $data = array(
                'title' => 'Dashboard',
                'content' => $this->load->view('pages/dashboard', $view_data, true),
            );
            $this->load->view('base/dashboard_template', $data);
        } else {
            redirect('login');
        }
    }

    public function get_events(){
        $result=$this->em->get_events();
        echo json_encode($result);
    }


  
}