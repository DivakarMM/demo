<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Events extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->is_logged_in();
        $this->load->library('form_validation');
        ($this->verify_min_level(1)) ? '' : redirect('login');
    }

    public function index()
    {
        $view_data['datatable'] = base_url() . 'users/events/datatable/';
        $data = array(
            'title' => 'Events',
            'content' => $this->load->view('events/show', $view_data, true),
        );
        $this->load->view('base/main_template', $data);
    }

    public function datatable()
    {
        $this->datatables->select('e.event_id as id,e.event_type,e.event_title,e.from_date,e.to_date,e.status,e.created_at')->from('events as e');
        $this->db->order_by("e.created_at", "desc");
        if ($this->auth_level != 9) {
            $this->db->where("e.user_id", $this->auth_user_id);
        }
        if ($this->auth_level == 9) {
            $this->datatables->add_column(
                'action',
                '<a class="btn btn-primary btn-sm" href="' . base_url() . 'users/events/edit/$1" title="Edit"><i class="fa fa-pen"></i></a>',
                'id'
            );
        } else {
            $this->datatables->add_column('action', 'Not Allowed');
        }
        $this->datatables->add_column('delete', '<a  class="btn btn-danger btn-sm"  href="javascript:void(0);"  onclick="delete_item($1)"  title="Delete"> <i class="fas fa-trash-alt"></i> </a>', 'id');
        echo $this->datatables->generate();
    }

    public function add()
    {
        $view_data = '';
        if (isset($_POST['submit'])) {
            //Receive Values
            $from_date = $this->input->post('from_date');
            $to_date = $this->input->post('to_date');
            $event_title = $this->input->post('event_title');

            //Set validation Rules
            $this->form_validation->set_rules('from_date', 'From Date', 'required');
            $this->form_validation->set_rules('to_date', 'To Date', 'required');
            $this->form_validation->set_rules('event_title', 'Event Title', 'required');
            //check is the validation returns no error
            if ($this->form_validation->run() == true) {
                $event_name = $this->mcommon->specific_row('events', array('event_title' => $event_title));
                //prepare insert array
                $event_array = array(
                    'user_id' => $this->auth_user_id,
                    'from_date' => $from_date,
                    'to_date' => $to_date,
                    'event_title' => $event_title,
                    'status' => 0,
                    'created_at' => date("Y-m-d h:i:s"),
                    'updated_at' => date("Y-m-d h:i:s"),
                );
                //insert values in database
                $insert = $this->mcommon->common_insert('events', $event_array);
                if ($insert) {
                        $user_data['content']="Event created";
                         $msg = $this->load->view('event_mail', $user_data, true);

                        $this->load->library('email');
                        $config['protocol'] = 'smtp';
                        $config['smtp_host'] = 'smtp-relay.sendinblue.com';
                        $config['smtp_port'] = '587';
                        $config['smtp_timeout'] = '7';
                        $config['smtp_user'] = 'smtp_usr_name';
                        $config['smtp_pass'] = 'smtp_pass';
                        $config['charset'] = 'utf-8';
                        $config['newline'] = "\r\n";
                        $config['mailtype'] = 'html'; // or html
                        $config['validation'] = true; // bool whether to validate email or not
                        $this->email->initialize($config);
                        $this->email->from('info@integrass.com');
                        $this->email->to("admin@gmail.com");
                        $this->email->subject('Event created');
                        $this->email->message($msg);
                        //$mail_status = $this->email->send();

                    $this->session->set_flashdata('alert_success', 'Events added successfully!');
                } else {
                    $this->session->set_flashdata('alert_danger', 'Something went wrong. Please try again later');
                }
            }
        }

        $data = array(
            'title' => 'Add Events',
            'content' => $this->load->view('events/add', $view_data, true),
        );
        $this->load->view('base/main_template', $data);
    }


    public function edit($id)
    {
        if (isset($_POST['submit'])) {
            //Receive Values
            $type = $this->input->post('type');
            $status = $this->input->post('status');
            //Set validation Rules
            $this->form_validation->set_rules('type', 'Event Type', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');
            //check is the validation returns no error
            if ($this->form_validation->run() == true) {
                //prepare update array
                $update_array = array(
                    'event_type' => $type,
                    'status' => $status,
                );
                //insert values in database
                $update = $this->mcommon->common_edit('events', $update_array, array('event_id' => $id));
                if ($update) {
                    $this->session->set_flashdata('alert_success', 'Event updated successfully!');
                    redirect('users/events/');
                } else {
                    $this->session->set_flashdata('alert_danger', 'Something went wrong. Please try again later');
                }
            }
        }
        $view_data['default'] = $this->mcommon->specific_row('events', array('event_id' => $id));
        $data = array(
            'title' => 'Edit Events',
            'content' => $this->load->view('events/edit', $view_data, true),
        );
        $this->load->view('base/main_template', $data);
    }

    public function delete($id)
    {
        $delete = $this->mcommon->common_delete('events', array('event_id' => $id));
        return $delete;
    }
}
