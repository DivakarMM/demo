<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manage_users extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->is_logged_in();
        $this->load->library('form_validation');
        ($this->verify_min_level(1)) ? '' : redirect('login');
    }

    public function index()
    {
        $view_data['datatable'] = base_url() . 'users/manage_users/datatable/';
        $data = array(
            'title' => 'User Management',
            'content' => $this->load->view('user/show', $view_data, true),
        );
        $this->load->view('base/main_template', $data);
    }

    public function datatable()
    {
        $this->datatables->select('u.user_id as id,u.username,u.email,u.mobile,u.created_at')->from('users as u');
        $this->db->order_by("u.created_at", "desc");
        $this->db->where('u.auth_level!=',9);
        $this->datatables->add_column(
            'action',
            '<a class="btn btn-primary btn-sm" href="' . base_url() . 'users/manage_users/edit/$1" title="Edit"><i class="fa fa-pen"></i></a> &nbsp;','id');
            $this->datatables->add_column('delete', '<a class="btn btn-danger btn-sm"  href="javascript:void(0);" onclick="delete_item($1)" title="Delete"> <i class="fas fa-trash-alt"></i></a>', 'id');
            echo $this->datatables->generate();
    }

    public function add()
    {
        $view_data = '';
        if (isset($_POST['submit'])) {
            //Receive Values
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $email = $this->input->post('email');
            $mobile_number = $this->input->post('mobile_number');
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $confirm_password = $this->input->post('confirm_password');

            if ($password == "" || $confirm_password != "") {
                $validation_rules[] = array(
                    'field' => 'password',
                    'label' => 'password',
                    'rules' => [
                        'trim',
                        'required',
                        'matches[password]',
                        'rules' => 'callback_valid_password',
                    ],
                    'errors' => [
                        'required' => 'The password field is required.',

                    ],
                );
                $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');
            }
            //Set validation Rules
            $this->form_validation->set_rules($validation_rules);
            $this->form_validation->set_rules('first_name', 'First Name', 'required');
            $this->form_validation->set_rules('email', 'Last Name', 'required');
            $this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required');
            $this->form_validation->set_rules('username', 'Username', 'required');
            //check is the validation returns no error
            if ($this->form_validation->run() == true) {

                $check_mobile_number = $this->mcommon->specific_row('users', array('mobile' => $mobile_number));
                $user_id = $this->get_unused_id();
                if (!empty($check_mobile_number)) {
                    $this->session->set_flashdata('alert_danger', 'Mobile Number Already Exist');
                } else {
                    //prepare insert array
                    $user_array = array(
                        'user_id' => $user_id,
                        'username' => $username,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'email' => $email,
                        'mobile' => $mobile_number,
                        'auth_level' => 1,
                        'passwd' => $this->hash_passwd($password),
                        'created_at' => date("Y-m-d h:i:s"),
                    );
                    //insert values in database
                    $insert = $this->mcommon->common_insert('users', $user_array);
                    //prepare update array
                    if (!$insert) {
                        $this->session->set_flashdata('alert_success', 'User added successfully!');
                    } else {
                        $this->session->set_flashdata('alert_danger', 'Something went wrong. Please try again later');
                    }
                }
            }
        }
        $data = array(
            'title' => 'Add User',
            'content' => $this->load->view('user/add', $view_data, true),
        );
        $this->load->view('base/main_template', $data);
    }



    public function edit($id)
    {
        if (isset($_POST['submit'])) {

            //Receive Values
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $email = $this->input->post('email');
            $mobile_number = $this->input->post('mobile_number');
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $confirm_password = $this->input->post('confirm_password');

            if ($password == "" || $confirm_password != "") {
                $validation_rules[] = array(
                    'field' => 'password',
                    'label' => 'password',
                    'rules' => [
                        'trim',
                        'required',
                        'matches[password]',
                        'rules' => 'callback_valid_password',
                    ],
                    'errors' => [
                        'required' => 'The password field is required.',
                    ],
                );
                $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');
            }

            //Set validation Rules
            $this->form_validation->set_rules($validation_rules);
            $this->form_validation->set_rules('first_name', 'First Name', 'required');
            $this->form_validation->set_rules('email', 'Last Name', 'required');
            $this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required');
            $this->form_validation->set_rules('username', 'Username', 'required');

            //check is the validation returns no error
            if ($this->form_validation->run() == true) {
                //image Upload

                //prepare update array
                $update_array = array(
                    'username' => $username,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'mobile' => $mobile_number,
                    'auth_level' => 1,
                    'passwd' => $this->hash_passwd($password),
                );

                //insert values in database
                $update = $this->mcommon->common_edit('users', $update_array, array('user_id' => $id));
                if ($update) {
                    $this->session->set_flashdata('alert_success', 'User updated successfully!');
                    redirect('users/manage_users/');
                } else {
                    $this->session->set_flashdata('alert_danger', 'Something went wrong. Please try again later');
                }
            }
        }

        $view_data['default'] = $this->mcommon->specific_row('users', array('user_id' => $id));
        $data = array(
            'title' => 'Edit User',
            'content' => $this->load->view('user/edit', $view_data, true),
        );
        $this->load->view('base/main_template', $data);
    }

    public function delete($id)
    {
        $delete = $this->mcommon->common_delete('users', array('user_id' => $id));
        return $delete;
    }

    public function hash_passwd($password, $random_salt = '')
    {
        // If no salt provided for older PHP versions, make one
        if (!is_php('5.5') && empty($random_salt)) {
            $random_salt = $this->random_salt();
        }
        // PHP 5.5+ uses new password hashing function
        if (is_php('5.5')) {
            return password_hash($password, PASSWORD_BCRYPT, ['cost' => 11]);
        }

        // PHP < 5.5 uses crypt
        else {
            return crypt($password, '$2y$10$' . $random_salt);
        }
    }

    public function valid_password($password = '')
    {
        $password = trim($password);
        $regex_lowercase = '/[a-z]/';
        $regex_uppercase = '/[A-Z]/';
        $regex_number = '/[0-9]/';
        $regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';
        if (empty($password)) {
            $this->form_validation->set_message('valid_password', 'The {field} field is required.');
            return false;
        }
        if (preg_match_all($regex_lowercase, $password) < 1) {
            $this->form_validation->set_message('valid_password', 'The {field} field must be at least one lowercase letter.');
            return false;
        }
        if (preg_match_all($regex_uppercase, $password) < 1) {
            $this->form_validation->set_message('valid_password', 'The {field} field must be at least one uppercase letter.');
            return false;
        }
        if (preg_match_all($regex_number, $password) < 1) {
            $this->form_validation->set_message('valid_password', 'The {field} field must have at least one number.');
            return false;
        }
        if (preg_match_all($regex_special, $password) < 1) {
            $this->form_validation->set_message('valid_password', 'The {field} field must have at least one special character.' . ' ' . htmlentities('!@#$%^&*()\-_=+{};:,<.>§~'));
            return false;
        }
        if (strlen($password) < 5) {
            $this->form_validation->set_message('valid_password', 'The {field} field must be at least 5 characters in length.');
            return false;
        }
        if (strlen($password) > 32) {
            $this->form_validation->set_message('valid_password', 'The {field} field cannot exceed 32 characters in length.');
            return false;
        }
        return true;
    }

    public function get_unused_id()
    {
        // Create a random user id between 1200 and 4294967295
        $random_unique_int = 2147483648 + mt_rand(-2147482448, 2147483647);

        // Make sure the random user_id isn't already in use
        $query = $this->db->where('user_id', $random_unique_int)
            ->get_where('users');

        if ($query->num_rows() > 0) {
            $query->free_result();

            // If the random user_id is already in use, try again
            return $this->get_unused_id();
        }

        return $random_unique_int;
    }
}
