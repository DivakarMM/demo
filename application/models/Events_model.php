<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Events_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * [record_counts description]
     * @param  [type] $user_id [users id]
     * @return [INT]   user's id [description]
     * @author Divakar
     */

    public function get_events()
    {
        $this->db->select('*');
        $this->db->from('events');
        if ($this->auth_level != 9) {
            $this->db->where("user_id", $this->auth_user_id);
        }
        $this->db->where("status", 1);
        $query = $this->db->get();
        $result = $query->result();
        $output = array();
        foreach ($result as $res) {
            $output[] = array(
                'title' => $res->event_title,
                'start' => $res->from_date,
                'end' => $res->to_date,
                'className' => 'red'
            );
        }
        return $output;
    }
}
