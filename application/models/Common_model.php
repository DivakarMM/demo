<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Common_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * [record_counts description]
     * @param  [type] $user_id [users id]
     * @return [INT]   user's id [description]
     * @author Divakar
     */

    public function record_counts($table)
    {
        $this->db->select('*');
        $this->db->from($table);
        $num_results = $this->db->count_all_results();
        return $num_results;
    }

    public function specific_record_counts($table, $constraint_array)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($constraint_array);
        $num_results = $this->db->count_all_results();
        return $num_results;
    }

    public function specific_record_counts_other($table, $constraint_array)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($constraint_array);
        $num_results = $this->db->count_all_results();
        return $num_results;
    }

    public function specific_row($table, $constraint_array = '')
    {
        $this->db->select('*');
        $this->db->from($table);
        if (!empty($constraint_array)) {
            $this->db->where($constraint_array);
        }
        $result = $this->db->get()->row_array();
        return $result;
    }

    public function specific_row_value($table, $constraint_array = '', $get_field)
    {
        $this->db->select($get_field);
        $this->db->from($table);
        if (!empty($constraint_array)) {
            $this->db->where($constraint_array);
        }
        $result = $this->db->get()->row_array();
        return $result[$get_field];
    }

    public function records_all($table, $constraint_array = '', $order_by = '')
    {
        $this->db->select('*');
        $this->db->from($table);
        if (!empty($constraint_array)) {
            $this->db->where($constraint_array);
        }
        if (!empty($order_by)) {
            $this->db->order_by($order_by);
        }
        $results = $this->db->get()->result();
        return $results;
    }

    public function specific_fields_records_all($table, $constraint_array = '', $get_field_array = '')
    {
        if (!empty($get_field_array)) {
            $this->db->select($get_field_array);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table);
        if (!empty($constraint_array)) {
            $this->db->where($constraint_array);
        }
        $results = $this->db->get()->result_array();
        return $results;
    }

    public function common_insert($table, $data)
    {
        $this->db->insert($table, $data);
        $result = $this->db->insert_id();
        return $result;
    }

    public function common_edit($table, $data, $where_array)
    {
        $this->db->trans_start();
        $this->db->update($table, $data, $where_array);
        $this->db->trans_complete();
        if ($this->db->affected_rows() == '1') {
            return true;
        } else {
            if ($this->db->trans_status() === false) {
                return false;
            }
            return true;
        }
    }

    public function common_delete($table, $where_array)
    {
        $this->db->delete($table, $where_array);
        if ($this->db->affected_rows() == '1') {
            return true;
        } else {
            return false;
        }
    }


}